import { Config, browser } from "protractor";
export let config: Config = {
    directConnect: true,
    specs: [
        "specs/**/*.spec.js"
    ],
    onPrepare: async () => {
        await browser.waitForAngularEnabled(false);
    }
}