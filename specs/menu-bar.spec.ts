import { browser, by, element } from 'protractor';
import { ezLogin } from '../home-page/quick-login';
import { homeUrl, mainUrl, aboutlink } from "../utils/websites";

const menuBar = element(by.id('react-burger-menu-btn'));

const allItems = element(by.id('inventory_sidebar_link'));
const about = element(by.id('about_sidebar_link'));
const logout = element(by.id('logout_sidebar_link'));
const reset = element(by.id('reset_sidebar_link'));

describe("menu", () => {
    it("all items button should work", () => {
        menuBar.click();
        browser.sleep(1000);
        allItems.click();
        expect(browser.getCurrentUrl()).toBe(homeUrl);
    });
    it("about button should work", () => {
        menuBar.click();
        browser.sleep(1000);
        about.click();
        expect(browser.getCurrentUrl()).toBe(aboutlink);
    });
    it("logout button should work", () => {
        menuBar.click();
        browser.sleep(1000);
        logout.click();
        expect(browser.getCurrentUrl()).toBe(mainUrl);
    });
    it("reset app state button should work", () => {
        menuBar.click();
        browser.sleep(1000);
        reset.click();
        expect(browser.getCurrentUrl()).toBe(homeUrl);
    });
});

beforeEach(() => {
    ezLogin();
});