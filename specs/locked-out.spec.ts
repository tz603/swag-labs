import { browser, element, by } from "protractor";
import { mainUrl } from "../utils/websites";

describe("lock out", () => {
    it("should state error when user tries to login", () => {
        browser.get(mainUrl);
        expect(browser.getCurrentUrl()).toBe(mainUrl);
        element(by.id('user-name')).sendKeys("lock_out_user");
        element(by.id('password')).sendKeys("secret_sauce");
        element(by.id('login-button')).click();
        expect(element(by.css('.error-message-container.error')).isPresent()).toBe(true);
    });
});