import { by, element, browser } from "protractor";
import { homeUrl, mainUrl } from "../utils/websites";

describe("problem", () => {
    it("should display problem when user tries to login", () => {
        browser.get(mainUrl);
        expect(browser.getCurrentUrl()).toBe(mainUrl);
        element(by.id('user-name')).sendKeys("problem_user");
        element(by.id('password')).sendKeys("secret_sauce");
        element(by.id('login-button')).click();
        expect(browser.getCurrentUrl()).toBe(homeUrl);
    });
});