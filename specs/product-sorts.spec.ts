import { $, $$ } from 'protractor';
import { ezLogin } from '../home-page/quick-login';

const sort = (o) => {
    $('option[value="' + o + '"]').click();
};

const getTitles = () => {
    return $$('.inventory_item_name').map((option) => {
        return option.getText();
      });
};
const getPrices = () => {
    return $$('.inventory_item_price').map((option) => {
        return option.getText()
          .then((optionText) => {
            return parseFloat(optionText.trim().replace('$',''));
          });
      });
};

const sorting = (items, order, type) => {
    let newarr = items.slice();
    if (type == 'title') {
        switch (order) {
            case 'az':
                return newarr.sort();
            case 'za':
                newarr.sort();
                return newarr.reverse();
            default:
                console.log('Sorry, I don\'t understand your titles');
        }
    } else if (type == 'price') {
        switch (order) {
            case 'hilo':
                return newarr.sort(function (a, b) { return b - a });
            case 'lohi':
                return newarr.sort(function (a, b) { return a - b });
            default:
                console.log('Sorry, I don\'t understand your prices');
        }
    }
};

describe("sorting", async () => {
    it("should display name a to z", async () => {
        sort('az');
        const sortedTitles = await getTitles();
        const mySortedItems = await sorting(sortedTitles, 'az', 'title');
        expect(sortedTitles).toEqual(mySortedItems);
    });
    it("should display name z to a", async () => {
        sort('za');
        const sortedTitles = await getTitles();
        const mySortedItems = await sorting(sortedTitles, 'za', 'title');
        expect(sortedTitles).toEqual(mySortedItems);
    });
    it("should display price low to high", async () => {
        sort('lohi');
        const sortedPrices = await getPrices();
        const mySortedPrices = await sorting(sortedPrices, 'lohi', 'price');
        expect(sortedPrices).toEqual(mySortedPrices);
    });
    it("should display high to low", async () => {
        sort('hilo');
        const sortedPrices = await getPrices();
        const mySortedPrices = await sorting(sortedPrices, 'hilo', 'price');
        expect(sortedPrices).toEqual(mySortedPrices);
    });
});

beforeEach(() => {
    ezLogin();
});