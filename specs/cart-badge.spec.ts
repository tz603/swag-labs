import { element, by, $, browser, protractor } from 'protractor';
import { ezLogin } from '../home-page/quick-login';

const EC = protractor.ExpectedConditions;
const add = element(by.id('add-to-cart-sauce-labs-backpack'));
const remove = element(by.id('remove-sauce-labs-backpack'));
const badge = $('.shopping_cart_badge');

describe("cart badge", () => {
    it("should show amount of items in cart after clicking add to cart", () => {
        browser.wait(EC.presenceOf(add), 1000, 'element take long than 1 second to appear');
        add.click();
        expect(badge.getText()).toEqual('1');
        browser.wait(EC.presenceOf(remove), 1000, 'element take long than 1 second to appear');
        remove.click();
        expect(badge.isPresent()).toBe(false);
    });
});


beforeEach(() => {
    ezLogin();
});