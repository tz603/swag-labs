import { theCart } from '../home-page/cart';
import { ezLogin } from "../home-page/quick-login";
import { cart } from "../utils/websites";

describe("login", () => {
    it("should check & go to cart", () => {
        const header = new theCart();
        header.checkButton();
        header.cartButton.click();
        header.checkUrl(cart);
    });
});

beforeEach(() => {
    ezLogin();
});