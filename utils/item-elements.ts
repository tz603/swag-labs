import { element, by, browser, $ } from "protractor";

const item1 = element(by.id('add-to-cart-sauce-labs-backpack'));
const item2 = element(by.id('add-to-cart-sauce-labs-bike-light'));
const item3 = element(by.id('add-to-cart-sauce-labs-bolt-t-shirt'));
const item4 = element(by.id('add-to-cart-sauce-labs-fleece-jacket'));
const item5 = element(by.id('add-to-cart-sauce-labs-onesie'));
const item6 = element(by.id('add-to-cart-test.allthethings()-t-shirt-(red)'));

const cartBadge = $('.shopping_cart_badge');

export function pick(){
    let items = [item1, item2, item3, item4, item5, item6],
    pickItem = items[Math.floor(Math.random() * items.length)];
    pickItem.click();
    browser.sleep(2000);
    expect(cartBadge.getText()).toEqual('1');
};