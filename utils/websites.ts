export const mainUrl: string = "https://www.saucedemo.com/";
export const homeUrl: string = "https://www.saucedemo.com/inventory.html";
export const cart: string = "https://www.saucedemo.com/cart.html";
export const aboutlink: string = "https://saucelabs.com/";