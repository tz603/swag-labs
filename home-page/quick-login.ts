import { browser, element, by } from "protractor";
import { homeUrl, mainUrl } from "../utils/websites";

export function ezLogin() {
    browser.get(mainUrl);
    expect(browser.getCurrentUrl()).toBe(mainUrl);
    element(by.id('user-name')).sendKeys("standard_user");
    element(by.id('password')).sendKeys("secret_sauce");
    element(by.id('login-button')).click();
    expect(browser.getCurrentUrl()).toBe(homeUrl);
};