import { element, by, browser } from "protractor";

export function theCart() {
    this.cartButton = element(by.id('shopping_cart_container'));
    this.checkButton = () => {
        expect(this.cartButton.isDisplayed()).toBe(true);
    };
    this.checkUrl = expectedUrl => {
        const currentUrl = browser.getCurrentUrl();
        expect(currentUrl).toBe(expectedUrl);
    };
};